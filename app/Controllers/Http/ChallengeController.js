'use strict'

const Challenge = use('App/Models/Challenge')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with challenges
 */
class ChallengeController {
  /**
   * Show a list of all challenges.
   * GET challenges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    
    //const challenges = await Challenge.all();
    const challenges = await Challenge.query().with('usuario').fetch();

    return challenges;

  }

  /**
   * Render a form to be used for creating a new challenge.
   * GET challenges/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /*async create ({ request, response, view }) {
  }*/

  /**
   * Create/save a new challenge.
   * POST challenges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, auth }) {
    const data = request.only(['number', 'description']);

    const challenge = await Challenge.create({user_id: auth.user.id, ...data});

    return challenge;
  }

  /**
   * Display a single challenge.
   * GET challenges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params }) {
    const challenge = await Challenge.findOrFail(params.id);

    return challenge;
  }

  /**
   * Render a form to update an existing challenge.
   * GET challenges/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /*async edit ({ params, request, response, view }) {
  }*/

  /**
   * Update challenge details.
   * PUT or PATCH challenges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request }) {
    const data = request.all();

    const challenge = await Challenge.findOrFail(params.id);

    challenge.merge(data);

    await challenge.save();

    return challenge;
  }

  /**
   * Delete a challenge with id.
   * DELETE challenges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, auth, response }) {
    const challenge = await Challenge.findOrFail(params.id);

    if(challenge.user_id != auth.user.id){
      return response.status('401');
    }

    await challenge.delete();
  }
}

module.exports = ChallengeController
