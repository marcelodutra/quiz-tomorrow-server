'use strict'

const Qualification = use('App/Models/Qualification');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with qualifications
 */
class QualificationController {
  /**
   * Show a list of all qualifications.
   * GET qualifications
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const qualifications = await Qualification.query().with('desafio').fetch();

    return qualifications;
  }

  /**
   * Render a form to be used for creating a new qualification.
   * GET qualifications/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /*async create ({ request, response, view }) {
  }*/

  /**
   * Create/save a new qualification.
   * POST qualifications
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const data = request.all();

    const qualification = Qualification.create(data);

    return qualification;
  }

  /**
   * Display a single qualification.
   * GET qualifications/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const qualification = await Qualification.findOrFail(params.id);

    return qualification;
  }

  /**
   * Render a form to update an existing qualification.
   * GET qualifications/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  /*async edit ({ params, request, response, view }) {
  }*/

  /**
   * Update qualification details.
   * PUT or PATCH qualifications/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const data = request.all();

    const qualification = await Qualification.findOrFail(params.id);

    qualification.merge(data);

    qualification.save();

    return qualification;
  }

  /**
   * Delete a qualification with id.
   * DELETE qualifications/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const qualification = await Qualification.findOrFail(params.id);
    qualification.delete();

    return response.status(200).send('Qualificação deletada');
  }
}

module.exports = QualificationController
