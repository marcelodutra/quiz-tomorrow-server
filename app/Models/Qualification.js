'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Qualification extends Model {
    desafio(){
        return this.belongsTo('App/Models/Challenge');
    }
}

module.exports = Qualification
