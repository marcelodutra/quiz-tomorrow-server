'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Question extends Model {
    desafio(){
        return this.belongsTo('App/Models/Challenge');
    }
}

module.exports = Question
