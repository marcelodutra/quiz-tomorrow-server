'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QualificationSchema extends Schema {
  up () {
    this.create('qualifications', (table) => {
      table.increments()
      table
          .integer('challenge_id')
          .unsigned()
          .notNullable()
          .references('id')
          .inTable('challenges')
          .onUpdate('CASCADE')
          .onDelete('CASCADE')
      table.string('description').notNullable()
      table.integer('star_quantity').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('qualifications')
  }
}

module.exports = QualificationSchema
