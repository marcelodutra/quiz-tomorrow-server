'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionSchema extends Schema {
  up () {
    this.create('questions', (table) => {
      table.increments()
      table
          .integer('challenge_id')
          .unsigned()
          .notNullable()
          .references('id')
          .inTable('challenges')
          .onUpdate('CASCADE')
          .onDelete('CASCADE')
      table.string('description').notNullable()
      table.integer('difficulty').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('questions')
  }
}

module.exports = QuestionSchema
