'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnswerSchema extends Schema {
  up () {
    this.create('answers', (table) => {
      table.increments()
      table
          .integer('question_id')
          .unsigned()
          .notNullable()
          .references('id')
          .inTable('questions')
          .onUpdate('CASCADE')
          .onDelete('CASCADE')
      table.string('description').notNullable()
      table.boolean('is_correct').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('answers')
  }
}

module.exports = AnswerSchema
